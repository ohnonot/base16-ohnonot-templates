#!/bin/bash

cat <<EOF
<h2 id="{{ scheme-slug }}">{{ scheme-slug }}<a href="#" class="toc-nav"> ∆</a></h2><div style="color:#{{base05-hex}}" class="flxbp">
EOF

for bg in 00 01 02 07; do
	printf '<div style="background:#{{base%s-hex}}"><p>bg: base%s</p>\n' $bg $bg
	for fg in 03 04 05 06 08 09 0A 0B 0C 0D 0E 0F; do
		printf '\t<span style="color:#{{base%s-hex}}">fg:%s</span> \n' $fg $fg
	done
	printf '</div>\n'
done

printf '</div>\n'
