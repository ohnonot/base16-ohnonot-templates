#!/bin/bash

# concatenates all previews to stdout
# needs to be run in the directory the html snippets are in
# option -f: create a full html document

# flex solution
#~ div.smallpreview {
    #~ width:100%;
    #~ height:2.5em;
    #~ display:flex
    #~ }
#~ div.smallpreview div {
    #~ display:flex;
    #~ flex:auto;
    #~ align-items:center;
    #~ justify-content: center;
    #~ font-weight: bold;
    #~ margin: .1em
    #~ }

[[ "$1" == "-f"* ]] && cat <<EOF || echo "<style>"
<html><head><meta http-equiv="content-type" content="text/html">
<style type="text/css" media="screen">
  body { margin: 5% 20%; }
EOF

cat <<EOF
pre, .smallpreview { font-family: monospace; font-size: 100% }
div.smallpreview div {
    display: inline-block;
    font-weight: bold;
    padding: .9em 1.1em;
    margin: 0 .5em .5em 0
    }
details > pre {
    padding: 10px 20px
}
summary > pre {
    cursor:pointer;
    padding: 10px 10px
}
summary::marker { display:none }
summary { list-style: none }
hr { margin: 0.5em 0 }
</style>
EOF
[[ "$1" == "-f"* ]] && echo "<body>"
#~ echo "<div id=toc><ul>"
#~ cat *.listitem
#~ echo "</ul></div>"
cat *.html
[[ "$1" == "-f"* ]] && echo "</body></html>"
exit 0
